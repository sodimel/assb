//
//	compile:
//		gcc assb.c -o assb
//	run:
//		./assb
//
#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <dirent.h> 
#include <stdlib.h>
#include <sys/time.h>
#include <stdbool.h>

void generateWebsite();
void generateIndex();
void generateCss();
void generatePages();
void listFiles();
void removeWebsite();

char* append2String(char *, char*);

void writeFileTo(char *, char *, unsigned char);
void writeLine(char *, char *, unsigned char);


struct timeval timeStart, timeEnd;
bool verbose = false;

int getDuration(){
	return ((timeEnd.tv_sec - timeStart.tv_sec) * 1000000 + timeEnd.tv_usec) - timeStart.tv_usec;
}

int main(int argc, char *argv[]){
	if(argc == 2 && (!strcmp(argv[1],"help") || !strcmp(argv[1],"--help") || !strcmp(argv[1],"-h"))){
		printf("Usage:\n  ./assb [help|--help|-h]\n    Display this message.\n\n  ./assb\n    Build the static website.\n\n  ./assb [verbose|--verbose|-v]\n    Enable verbose mode (show files deleted/created).");
		return EXIT_SUCCESS;
	}

	if(argc == 2 && (!strcmp(argv[1], "verbose") || !strcmp(argv[1], "--verbose") || strcmp(argv[1], "-v"))){
		verbose = true;
		printf("Verbose mode set to true.\n");
	}


	gettimeofday(&timeStart, NULL);

	printf("Removing old files inside \"./public/article\" directory...\n");

	if(verbose)
		printf("\n");

	removeWebsite();

	if(verbose)
		printf("\n");

	printf("Generating website...\n");

	if(verbose)
		printf("\n");

	generateWebsite();

	if(verbose)
		printf("\n");

	gettimeofday(&timeEnd, NULL);
	printf("Generation completed (%ius).\n", getDuration());


	return EXIT_SUCCESS;
}


void generateWebsite(){
	generateIndex();

	if(verbose)
		printf("\n");

	printf("  Index completed.\n");

	if(verbose)
		printf("\n");

	generateCss();

	if(verbose)
		printf("\n");

	printf("  Css completed.\n");

	if(verbose)
		printf("\n");

	generatePages();
}


void generateIndex(){
	writeFileTo("template/header.html", "public/index.html", 1);
	writeFileTo("template/headerindex.html", "public/index.html", 0);
	listFiles();
	writeFileTo("template/footerindex.html", "public/index.html", 0);
	writeFileTo("template/footer.html", "public/index.html", 0);
}


void generateCss(){
	writeFileTo("template/design.css", "public/design.css", 1);
}


void generatePages(){
	DIR *d;
	struct dirent *dir;
	d = opendir("content");
	if(d){
		while ((dir = readdir(d)) != NULL){
			if(dir->d_type == DT_REG){
				char * from = append2String("content/", dir->d_name);
				char * to = append2String("public/article/", dir->d_name);

				writeFileTo("template/header.html", to, 1);
				writeFileTo(from, to, 0);
				writeFileTo("template/footer.html", to, 0);
				if(verbose)
    				printf("    %s completed.\n", dir->d_name);
			}
		}
		closedir(d);
	}
}


void listFiles(){
	DIR *d;
	struct dirent *dir;
	d = opendir("content");
	if(d){
		while ((dir = readdir(d)) != NULL){
			if(dir->d_type == DT_REG){
				char * s1 = append2String("<li><a href=\"article/", dir->d_name);
				char * s2 = append2String(s1, "\">");
				char * s3 = append2String(s2, dir->d_name);
				char * s4 = append2String(s3, "</a></li>");
				writeLine("public/index.html", s4, 0);
			}
		}
		closedir(d);
	}
}

void removeWebsite(){
	DIR *d;
	struct dirent *dir;
	d = opendir("public/article");
 
	while ((dir = readdir(d)) != NULL){
		if(dir->d_type == DT_REG){
			char fullpath[269] = "public/article/";
			strcat(fullpath, dir->d_name);
			if(remove(fullpath) == 0){
				if(verbose)
					printf("    deleted %s\n", fullpath);
			}
			else
				printf("    error encountered, cannot delete %s\n", fullpath);
		}
	}
}

char * append2String(char *s1, char *s2){
	char * result = NULL;
	asprintf(&result, "%s%s", s1, s2);
	return result;
}


void writeFileTo(char *nameIn, char *nameOut, unsigned char truncate){
	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	fp = fopen(nameIn, "r");
	if(fp == NULL)
		exit(EXIT_FAILURE);

	while((read = getline(&line, &len, fp)) != -1){
		writeLine(nameOut, line, truncate);
		if(truncate == 1)
			truncate = 0;
	}
	if(line)
		free(line);

	fclose(fp);

	if(verbose)
		printf("    %s -> %s\n", nameIn, nameOut);
}


void writeLine(char *name, char *content, unsigned char truncate){
	FILE* fichier = NULL;

	if(!truncate){
		fichier = fopen(name, "a");
	}
	else
		fichier = fopen(name, "w+");

	if(fichier != NULL){
		fputs(content, fichier);
		fclose(fichier);
	}
}