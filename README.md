# Another Static Site Builder

I don't need a heavy RUBY/JS/JAVA/[...] program only to concatenate 3 files and store the result in a directory.



<details>
<summary>Rapid links</summary>

 * [The idea](#the-idea)
 * [The structure](#the-structure)
 * [How to use](#how-to-use)
 * [Status](#status) - [Video ?](#video-)
 * [Picture after writing some css ?](picture-after-writing-some-css-)

</details>

## The idea:

Use a simple C program to build a simple static website from a simple structure.

![Structure](https://i.imgur.com/axR2OaQ.png)

## The structure:

A website have three directories and some files inside them:
- `template/`
	- `header.html` : general header
	- `footer.html` : general footer
	- `headerindex.html` : header for listing pages feature
	- `footerindex.html` : footer for listing pages feature
	- `design.css` : css file
- `content/`
	- `article.html` : a page of the website
- `public/`
	- `index.html` : (generated) list of links to each article
	- `article/` : where the pages will be saved

## How to use:

- Create files (see left part of image in [The idea](#the-idea) section), and take a look at [assb-example](https://gitlab.com/sodimel/assb-example)).
- Compile project:
	Run `gcc assb.c -o assb` or `make`.
- Create site:
	- Run `assb` or `make run`.
	- Website is available at `public/index.html`.
- Add an article:
	- Create a new html file inside `content`.
	- Run `assb` or `make run`.
	- Content is available at `public/content/newhtmlfile.html`.
- Remove an article:
	- Just remove the file from `content/` folder and run `assb` or `make run`.

## Status:

- [x] Start to think about the project
	- [x] Find idea of what to do
		- [x] It's a C project
		- [x] For Linux
		- [x] Source files are inside `template` & `content`, website is in `public` directory
- [x] Write README.md
- [x] First working version
- [x] Remove old files on each new build
- [x] Verbose mode
- [ ] Parse files and replace variables

## Picture after writing some css ?

Sure.

![Personnal project using assb](https://i.imgur.com/dclDxlF.png)

## Can I use assb to build my static website using gitlab-ci & gitlab pages ?

Sure, take a look at [this ugly guide](https://i.imgur.com/rGygL5H.png), see the [.gitlab-ci.yml file](https://gitlab.com/sodimel/infos/blob/88b60db4099ed8759d413af1b1f48d99d694bfe4/.gitlab-ci.yml) of the [infos-web](https://gitlab.com/sodimel/infos/) repo, and here's a [previous job](https://gitlab.com/sodimel/infos/-/jobs/399190994) log.
