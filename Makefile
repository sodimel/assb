CC = gcc
SRC = assb.c
PROG = assb

all: $(PROG)

$(PROG) : $(SRC)
	$(CC) $(SRC) -o $(PROG)

run:
	./$(PROG)

clean:
	rm assb

mrproper: clean
